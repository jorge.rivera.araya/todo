

export const addTodo = (title,comment)=> {
    return function (dispatch,getState) {
        dispatch({type:'ADD_TODO',title,comment});
        //dispatch(setSelectedRowId(null));
    }
}

export const saveTodo = (id,title,comment)=> {
  return function (dispatch,getState) {
      dispatch({type:'SAVE_TODO',id,title,comment});
      dispatch(setOption(null));
      dispatch(setSelectedRowId(null));
  }
}

export const deleteTodo = (id)=> {
  return function (dispatch,getState) {
      dispatch({type:'DELETE_TODO',id});
      dispatch(setOption(null));
      dispatch(setSelectedRowId(null));
  }
}

export const setVisibilityFilter = filter => ({
  type: 'SET_VISIBILITY_FILTER',
  filter
})

export const setSelectedRowId = (id)=> {
  return function (dispatch,getState) {
      dispatch({type:'SET_SELECTED_ROW_ID',id});
  }
}

export const toggleTodo = (id)=> {
  return function (dispatch,getState) {
      dispatch({type:'TOGGLE_TODO',id});
      dispatch(setSelectedRowId(null));
  }
}

export const setOption = (option)=> {
  return function (dispatch,getState) {
      dispatch({type:'SET_OPTION',option});
  }
}

export const setSearch = (search)=> {
  return function (dispatch,getState) {
      dispatch({type:'SET_SEARCH',search});
  }
}

export const setCancel = ()=> {
  return function (dispatch,getState) {
    dispatch(setOption(null));
    dispatch(setSelectedRowId(null));
  }
}