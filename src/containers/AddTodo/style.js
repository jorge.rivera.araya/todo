import styled from 'styled-components'

export const Box = styled.div`
    background-color: #f4f5f7;
    padding: 10px 10px 10px 10px;
    -webkit-border-radius: 5px;
    border-radius: 5px;
    color: #989595;
    margin-top:5px;
    font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen,Ubuntu,'Fira Sans','Droid Sans','Helvetica Neue',sans-serif;
    font-size:14px
`