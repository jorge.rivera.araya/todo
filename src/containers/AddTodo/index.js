import React,{Component} from 'react'
import { connect } from 'react-redux'
import {Box} from './style'
import Form from '../../components/Form'
import {setCancel} from '../../actions'

class AddTodo extends Component {
  constructor() {
    super();
    this.state = {
        write:false
    };
  } 

  cancel()
  {
    //this.props.cancel_todo();
    this.setState({write:false});
  }

  render() {
    return(
    <div>
      {
        this.state.write === true? 
        <Form 
          onSave={(title,comment)=>{this.props.add(title,comment);this.setState({write:false});}} 
          buttonText={"Add Task"} 
          title={""} 
          comment={""} 
          onCancel={()=>{this.cancel();}}
        /> 
        : 
        <Box onClick={()=>{this.setState({write:true});}}><span></span><span>Write here what to do...</span></Box>
      }
    </div>);
  }
}

const mapStateToProps = (state) => {
  return {
      option:state.option
  }
};
const mapDispatchToProps = (dispatch) => {
  return {
      add:(title,comment)=>{dispatch({type:'ADD_TODO',title,comment});},
      //cancel_todo:()=>{dispatch(setCancel());}
  }
};

export default connect(mapStateToProps,mapDispatchToProps,)(AddTodo)