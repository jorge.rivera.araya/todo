import { connect } from 'react-redux'
import TodoList from '../components/TodoList'
import CONSTANTES from '../CONSTANTES'

const getVisibleTodos = (_todos, filter, search) => {

  let todos = _todos.filter(t => t.title.indexOf(search) !== -1 || t.comment.indexOf(search) !== -1)

  switch (filter) {
    case CONSTANTES.SHOW_ALL:
      return todos
    case CONSTANTES.SHOW_COMPLETED:
      return todos.filter(t => t.completed)
    case CONSTANTES.SHOW_ACTIVE:
      return todos.filter(t => !t.completed)
    default:
      throw new Error('Unknown filter: ' + filter)
  }
}

const mapStateToProps = state => ({
  todos: getVisibleTodos(state.todos, state.visibilityFilter,state.search)
})

export default connect(
  mapStateToProps,
  null
)(TodoList)
