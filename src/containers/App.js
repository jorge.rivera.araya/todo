import React from 'react'
import AddTodo from './AddTodo'
import TodoList from './TodoList'
import Header from '../components/Header'

const App = () => (
  <div>
    <Header/>
    <AddTodo />
    <TodoList />
  </div>
)

export default App
