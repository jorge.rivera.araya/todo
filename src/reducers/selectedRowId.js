const selectedRowId = (state = null, action) => {
    switch (action.type) {
      case 'SET_SELECTED_ROW_ID':
        return action.id
      default:
        return state
    }
  }
export default selectedRowId