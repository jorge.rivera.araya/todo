import { combineReducers } from 'redux'
import todos from './todos'
import visibilityFilter from './visibilityFilter'
import selectedRowId from './selectedRowId'
import option from './option'
import search from './search'

export default combineReducers({
  todos,
  visibilityFilter,
  selectedRowId,
  option,
  search
})
