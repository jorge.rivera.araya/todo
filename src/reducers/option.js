const option = (state = null, action) => {
    switch (action.type) {
      case 'SET_OPTION':
        return action.option
      default:
        return state
    }
  }
export default option