const todos = (state = [], action) => {
    switch (action.type) {
      case 'DELETE_TODO':
        return state.filter(todo =>(todo.id !== action.id))
      break;
      case 'SAVE_TODO':
        return state.map(todo =>
          (todo.id === action.id)
            ? {...todo, comment: action.comment,title:action.title}
            : todo
        )
      break;
      case 'ADD_TODO':
        return [
          ...state,
          {
            id: (new Date()).getTime(),
            title: action.title,
            comment: action.comment,
            completed: false
          }
        ]
      case 'TOGGLE_TODO':
        return state.map(todo =>
          (todo.id === action.id)
            ? {...todo, completed: !todo.completed}
            : todo
        )
      default:
        return state
    }
  }
  
  export default todos
  