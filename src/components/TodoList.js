import React from 'react'
import PropTypes from 'prop-types'
import Todo from './Todo'

const TodoList = ({ todos }) => (
  <div style={{backgroundColor:"#f4f5f7"}}>
    {
    todos.map(todo =>
      <Todo key={todo.id} {...todo}/>
    )}
  </div>
)

TodoList.propTypes = {
  todos: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    completed: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
    comment: PropTypes.string.isRequired
  }).isRequired).isRequired
}

export default TodoList
