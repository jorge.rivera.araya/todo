import React,{Component} from 'react'
import PropTypes from 'prop-types'
import {Div,Estado,Titulo,Comment,Opciones,Button,Span,Button2} from './styles'
import {setSelectedRowId,toggleTodo,setOption,saveTodo,deleteTodo,setCancel} from '../../actions'
import { connect } from 'react-redux'
import Form from '../../components/Form'
import CONSTANTES from '../../CONSTANTES'

class Todo extends Component {
  constructor() {
    super();
    this.state = {
        options:false,
        delete:false
    };
  } 

  save(title,comment)
  {
    this.props.save_todo(this.props.id,title,comment);
  }

  cancel()
  {
    this.props.cancel_todo();
  }

  render() {
    const edit_selected = this.props.option===CONSTANTES.EDIT&&this.props.selected===this.props.id;
    return(
      <Div padding={edit_selected?"10px 0px 10px 0px":"10px 10px 10px 15px"}>
        {edit_selected ?
          <Form 
            onSave={(title,comment)=>{this.save(title,comment);}} 
            buttonText={"Save Task"} 
            title={this.props.title} 
            comment={this.props.comment} 
            onCancel={()=>{this.cancel()}}
          />
          :
          <div>
            <div onClick={()=>{this.props.selectedRowId(this.props.id)}}>
              <Titulo>{this.props.title}</Titulo> 
              <Comment>{this.props.comment}</Comment>
              <Estado color={this.props.completed===true?"#27c20f":"#ff9933"}/>
            </div>
            {this.props.selected===this.props.id &&
            <div>
              {this.state.delete===false?
              <Opciones>
                <Button onClick={()=>{this.setState({delete:true});}} color={"#ffffff"}><Span>Delete</Span></Button>
                <Button onClick={()=>{this.props.set_option(CONSTANTES.EDIT)}} color={"#ffffff"}><Span>Edit</Span></Button>
                <Button onClick={()=>{this.props.toggleTodo(this.props.id)}} color={"#ffffff"}><Span>{this.props.completed===true?"active!":"complete!"}</Span></Button>
                <Button onClick={()=>{this.props.selectedRowId(null)}} color={"#ffffff"}><Span>Cancel</Span></Button>
              </Opciones>
              :
              <div style={{marginTop:5}}>
                <Span>Are you sure delete?</Span>
                <Button2 active={true} onClick={()=>{this.props.delete_todo(this.props.id)}}>Delete</Button2>
                <Button2 active={false} onClick={()=>{this.props.selectedRowId(null);this.setState({delete:false});}}>Cancel</Button2>
              </div>
              }
            </div>
            }
          </div>
        }
      </Div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
      selected:state.selectedRowId,
      option:state.option
  }
};
const mapDispatchToProps = (dispatch) => {
  return {
    selectedRowId:(id)=>{dispatch(setSelectedRowId(id));},
    toggleTodo:(id)=>{dispatch(toggleTodo(id));},
    set_option:(option)=>{dispatch(setOption(option));},
    save_todo:(id,title,comment)=>{dispatch(saveTodo(id,title,comment));},
    delete_todo:(id)=>{dispatch(deleteTodo(id));},
    cancel_todo:()=>{dispatch(setCancel());},
  }
};

Todo.propTypes = {
  completed: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  comment: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired
}

export default connect(mapStateToProps,mapDispatchToProps,)(Todo)


/*
const Todo = ({ onClick, completed, title,comment }) => (
  
)*/

