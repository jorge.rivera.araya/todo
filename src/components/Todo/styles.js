import styled,{keyframes} from 'styled-components'

const fadeInKeyFrames = keyframes`
    from{
        filter:blur(2px);
        opacity:0.5
    }
    to{
        filter:blur(0);
        opacity:1
    }
`

export const Opciones = styled.div`
    margin-bottom:5px;
`

export const Div = styled.div`
   animation: 0.5s ${fadeInKeyFrames} ease;
   border:0;
   border-radius: 2px;
   box-shadow: 0px 1px 2px 0px rgba(9, 30, 66, 0.25);
   background: #fff;
   color: #333;
   font-size: 14px;
   margin-top: 5px;
   margin-bottom: 5px;
   padding: ${props => props.padding};
   position: relative;
   transition: background-color 140ms ease-in-out, border-color 75ms ease-in-out;
   display: block;
   list-style: none;
   border-spacing: 10px 0;
   line-height: 1.42857143;
   cursor:pointer;
    `

export const Estado = styled.div`
   background-color:${props => props.color};
   width: 3px;
    left: 5px;
    top: 5px;
    bottom: 5px;
    height: auto;
    position: absolute;
    text-indent: -9999em;
    margin: 0;
    padding: 0;
`

export const Titulo = styled.section`
   font-size: 16px;
   color: #172B4D;
   font-weight: 400;
   font-style: normal;
   font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen,Ubuntu,'Fira Sans','Droid Sans','Helvetica Neue',sans-serif;
   margin-bottom:2px;
   text-transform:capitalize;
`

export const Comment = styled.span`
   line-height: 1.42857143;
   color: #999;
   /*font-style: italic;*/
   font-size: 13px;
   font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen,Ubuntu,'Fira Sans','Droid Sans','Helvetica Neue',sans-serif;
   text-transform:capitalize;
`

export const Button = styled.button`
    padding: 0 10px 0 10px;
    text-decoration: none;
    max-width: 250px;
    text-overflow: ellipsis;
    overflow: hidden;
    &:hover {
        text-decoration: inherit;
        cursor: pointer;
        background: rgb(235,236,240)
    }
    -webkit-box-align: baseline;
    align-items: baseline;
    box-sizing: border-box;
    display: inline-flex;
    font-size: inherit;
    font-style: normal;
    font-weight: normal;
    text-align: center;
    white-space: nowrap;
    color: rgb(80, 95, 121);
    cursor: default;
    height: 2.28571em;
    line-height: 2.28571em;
    vertical-align: middle;
    width: auto;
    border-width: 0px;
    background: ${props => props.color};
    border-radius: 3px;
    transition: background 0.1s ease-out 0s, box-shadow 0.15s cubic-bezier(0.47, 0.03, 0.49, 1.38) 0s;
    outline: none !important;
`

export const Span = styled.span`
    align-self: center;
    display: inline-flex;
    flex-wrap: nowrap;
    max-width: 100%;
    position: relative;
    &:hover {
        cursor: pointer;
    }
    font-size: inherit;
    font-style: normal;
    font-weight: normal;
    text-align: center;
    white-space: nowrap;
    color: rgb(80, 95, 121);
    cursor: default;
    line-height: 2.28571em;
    font: 400 13.3333px Arial;
    font-size: 14px;
`


export const Button2 = styled.button`
    color: ${props => props.active ? "#fff" : "#000000a6"};      
    background-color: ${props => props.active ? "#1890ff" : "#ffffff"};
    /*border-color: ${props => props.active ? "" : "#d9d9d9"};*/
    text-shadow: 0 -1px 0 rgba(0,0,0,0.12);
    -webkit-box-shadow: 0 2px 0 rgba(0,0,0,0.045);
    box-shadow: 0 2px 0 rgba(0,0,0,0.045);

    line-height: 1.499;
    position: relative;
    display: inline-block;
    font-weight: 400;
    white-space: nowrap;
    text-align: center;
    background-image: none;
    border: ${props => props.active ? "1px solid transparent" : "1px solid #d9d9d9"};
    transition: all .3s cubic-bezier(.645, .045, .355, 1);
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    touch-action: manipulation;
    height: 32px;
    padding: 0 15px;
    font-size: 14px;
    border-radius: 4px;
    width:100%;
    margin-right:0px;
    margin-top:10px;
    @media only screen and (min-width: 500px) {
        width:100px;
    };
    cursor:pointer !important;

    margin-left:10px;
`