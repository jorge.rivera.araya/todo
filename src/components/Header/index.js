import React,{Component} from 'react'
import PropTypes from 'prop-types'
import {Button,Span,Input,Options,Row} from './styles'
import CONSTANTES from '../../CONSTANTES'
import {setVisibilityFilter,setSearch} from '../../actions'
import { connect } from 'react-redux'

class index extends Component {
  render() {
    const filter = this.props.filter;

    return(
      <Row>
        <Input value={this.props.search} placeholder={"Search..."} onChange={(v)=>{this.props.set_search(v.target.value);}}/>
        <Options>
          <Button onClick={()=>{this.props.visibilityFilter(CONSTANTES.SHOW_ALL)}} color={filter===CONSTANTES.SHOW_ALL?"#ebecf0":"#ffffff"}><Span>All</Span></Button>
          <Button onClick={()=>{this.props.visibilityFilter(CONSTANTES.SHOW_ACTIVE)}} color={filter===CONSTANTES.SHOW_ACTIVE?"#ebecf0":"#ffffff"}><Span>Active</Span></Button>
          <Button onClick={()=>{this.props.visibilityFilter(CONSTANTES.SHOW_COMPLETED)}} color={filter===CONSTANTES.SHOW_COMPLETED?"#ebecf0":"#ffffff"}><Span>Completed</Span></Button>
        </Options>
      </Row>
    );
  }
}

const mapStateToProps = (state) => {
  return {
      filter:state.visibilityFilter,
      search:state.search
  }
};
const mapDispatchToProps = (dispatch) => {
  return {
    visibilityFilter:(filter)=>{dispatch(setVisibilityFilter(filter));},
    set_search:(search)=>{dispatch(setSearch(search));},
  }
};

index.propTypes = {
  filter: PropTypes.string.isRequired
}

export default connect(mapStateToProps,mapDispatchToProps,)(index)