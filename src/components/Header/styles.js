import styled from 'styled-components'

export const Button = styled.button`
    padding: 0 10px 0 10px;
    text-decoration: none;
    max-width: 250px;
    text-overflow: ellipsis;
    overflow: hidden;
    &:hover {
        text-decoration: inherit;
        cursor: pointer;
        background: rgb(235,236,240)
    }
    -webkit-box-align: baseline;
    align-items: baseline;
    box-sizing: border-box;
    display: inline-flex;
    font-size: inherit;
    font-style: normal;
    font-weight: normal;
    text-align: center;
    white-space: nowrap;
    color: rgb(80, 95, 121);
    cursor: default;
    height: 2em;
    line-height: 2.28571em;
    vertical-align: middle;
    width: auto;
    border-width: 0px;
    background: ${props => props.color};
    border-radius: 8px;
    transition: background 0.1s ease-out 0s, box-shadow 0.15s cubic-bezier(0.47, 0.03, 0.49, 1.38) 0s;
    outline: none !important;
`

export const Row = styled.div`
    @media only screen and (min-width: 430px) {
        display:flex;
    }
`

export const Options = styled.div`
    width:100%;
    margin-top:10px;
    margin-bottom:6px;
    min-width:200px;
    @media only screen and (min-width: 430px) {
        width:200px;
        margin-top:0px;
        margin-bottom:0px;
    }
`

export const Span = styled.span`
    align-self: center;
    display: inline-flex;
    flex-wrap: nowrap;
    max-width: 100%;
    position: relative;
    &:hover {
        cursor: pointer;
    }
    font-size: inherit;
    font-style: normal;
    font-weight: normal;
    text-align: center;
    white-space: nowrap;
    color: rgb(80, 95, 121);
    cursor: default;
    line-height: 2.28571em;
    font: 400 13.3333px Arial;
    font-size: 14px;
`

export const Input = styled.input`
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    margin: 0px 10px 0px 0px;
    padding: 0;
    font-variant: tabular-nums;
    list-style: none;
    -webkit-font-feature-settings: 'tnum';
    font-feature-settings: 'tnum';
    position: relative;
    display: inline-block;
    width: 100%;
    height: 32px;
    padding: 4px 11px;
    color: rgba(0,0,0,0.65);
    font-size: 14px;
    line-height: 1.5;
    background-color: #fff;
    background-image: none;
    border: 1px solid #d9d9d9;
    border-radius: 8px;
    -webkit-transition: all .3s;
    transition: all .3s;
    @media only screen and (min-width: 430px) {
        width:200px;
    }
   
`


