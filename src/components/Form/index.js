import React from 'react'
import PropTypes from 'prop-types'
import {Label,Field,Input,Comment,Button,Formulario,Options,Row} from './styles'

const Form = ({ onSave,title,comment,buttonText,onCancel}) => {
    let _title=null;
    let _comment=null;
    return(
    <Formulario style={{marginTop:4,backgroundColor:"#f4f5f7",padding:10}}>
        <form onSubmit={e => {
            e.preventDefault()
            if (!_title.value.trim()) {
                return;
            }
            onSave(_title.value,_comment.value);
        }}>
            <Row>
                <Label>Task</Label>
                <Field><Input autoFocus={true} ref={a => _title = a} defaultValue={title}/></Field>
            </Row>
            <Row>
                <Label>Comment</Label>
                <Field><Comment ref={a => _comment = a}  defaultValue={comment}/></Field>
            </Row>
            <Options>
                <Button type="submit" active={true}>{buttonText}</Button>
                <div style={{width:10}}></div>
                <Button onClick={()=>{onCancel();}} active={false}>Cancel</Button>
            </Options>
        </form>
    </Formulario>
    )
}

Form.propTypes = {
    onSave: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    comment: PropTypes.string.isRequired,
    buttonText: PropTypes.string.isRequired
}

export default Form
