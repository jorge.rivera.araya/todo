import styled,{keyframes} from 'styled-components'

const fadeInKeyFrames = keyframes`
    from{
        filter:focus(1px);
        opacity:0.5
    }
    to{
        filter:focus(0);
        opacity:1
    }
`
export const Formulario = styled.div`
    animation: 0.2s ${fadeInKeyFrames} ease;
`

export const Label = styled.div`
    display: block;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    width: 25%;
    -webkit-box-flex: 0;
    max-width:100px;
    line-height: 40px;
    color:rgb(80,95,121);
    font-family: Arial;
    font-size:14px;
`

export const Field = styled.div`
    display: block;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    width: 100%;
    @media only screen and (min-width: 300px) {
        width: 75%;
    }
`


export const Row = styled.div`
    @media only screen and (min-width: 300px) {
        display:flex;
    }
`
export const Options = styled.div`
    @media only screen and (min-width: 300px) {
        display:flex;
    }
`

export const Input = styled.input`
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    margin: 0;
    padding: 0;
    font-variant: tabular-nums;
    list-style: none;
    -webkit-font-feature-settings: 'tnum';
    font-feature-settings: 'tnum';
    position: relative;
    display: inline-block;
    width: 100%;
    @media only screen and (min-width: 500px) {
        width:200px;
    }
    height: 32px;
    padding: 4px 11px;
    color: rgba(0,0,0,0.65);
    font-size: 14px;
    line-height: 1.5;
    background-color: #fff;
    background-image: none;
    border: 1px solid #d9d9d9;
    border-radius: 4px;
    -webkit-transition: all .3s;
    transition: all .3s;
`

export const Comment = styled.textarea`
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    margin: 0;
    padding: 0;
    font-variant: tabular-nums;
    list-style: none;
    -webkit-font-feature-settings: 'tnum';
    font-feature-settings: 'tnum';
    position: relative;
    display: inline-block;
    width: 100%;
    @media only screen and (min-width: 500px) {
        width:250px;
    }
    height: 60px;
    padding: 4px 11px;
    color: rgba(0,0,0,0.65);
    font-size: 14px;
    line-height: 1.5;
    background-color: #fff;
    background-image: none;
    border: 1px solid #d9d9d9;
    border-radius: 4px;
    -webkit-transition: all .3s;
    transition: all .3s;
`

export const Button = styled.button`
    color: ${props => props.active ? "#fff" : "#000000a6"};      
    background-color: ${props => props.active ? "#1890ff" : "#ffffff"};
    /*border-color: ${props => props.active ? "" : "#d9d9d9"};*/
    text-shadow: 0 -1px 0 rgba(0,0,0,0.12);
    -webkit-box-shadow: 0 2px 0 rgba(0,0,0,0.045);
    box-shadow: 0 2px 0 rgba(0,0,0,0.045);

    line-height: 1.499;
    position: relative;
    display: inline-block;
    font-weight: 400;
    white-space: nowrap;
    text-align: center;
    background-image: none;
    border: ${props => props.active ? "1px solid transparent" : "1px solid #d9d9d9"};
    transition: all .3s cubic-bezier(.645, .045, .355, 1);
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    touch-action: manipulation;
    height: 32px;
    padding: 0 15px;
    font-size: 14px;
    border-radius: 4px;
    width:100%;
    margin-right:0px;
    margin-top:10px;
    @media only screen and (min-width: 500px) {
        width:100px;
    };
    cursor:pointer !important;
`

